# # LJ - Graph

## Visit :  [LJ-Graph](https://caiquesarkis.gitlab.io/lj-graph)   

## Author: Caique Sarkis ([@caique_sarkis](https://twitter.com/caique_sarkis))

## Description: This is a simple simulation that uses Lennard Jones potential to show its properties.

## Reference:



[LJ Potential ](https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Physical_Properties_of_Matter/Atomic_and_Molecular_Properties/Intermolecular_Forces/Specific_Interactions/Lennard-Jones_Potential)   

