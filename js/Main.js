const aspectRatio =  window.innerWidth / window.innerHeight
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, aspectRatio, 0.1, 1000 );
const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
scene.background = new THREE.Color(0x03071e );
document.getElementById("container").appendChild(renderer.domElement )


// Variables
let Graph;
var ctx = document.getElementById('graph').getContext('2d');
let colors = [0x03071e]
let running = false;
let stepsPerFrame = 25;
let dt = 0.001;
let radius = 0.5;
let initDist = parseFloat(document.getElementById("initialD").value);
let oldPosition = [new THREE.Vector3(radius+initDist,0,0),new THREE.Vector3(-(radius+initDist),0,0)];
let position = [new THREE.Vector3(radius+initDist,0,0),new THREE.Vector3(-(radius+initDist),0,0)];
let velocity = [new THREE.Vector3(),new THREE.Vector3()];
let aceleration = [new THREE.Vector3(),new THREE.Vector3()];


// LJ variables
let epslon = 1;
let sigma = 2*radius;


// Camera movement
let camP = 5;
let camV = 0.001;

// Objects

// Grid
const size = 50;
const divisions = 100;
const gridHelper = new THREE.GridHelper( size, divisions ,0xd00000,0x370617);
gridHelper.rotation.x -= Math.PI/2 
//scene.add( gridHelper );


// LJ Spheres
const geometry = new THREE.SphereGeometry(radius,7,7);
const material = new THREE.MeshBasicMaterial({color:0xfaa307,wireframe:true});

const sphere1 = new THREE.Mesh(geometry,material);
const sphere2 = new THREE.Mesh(geometry,material);

scene.add(sphere1);
scene.add(sphere2);

sphere1.position.copy(position[0]);
sphere2.position.copy(position[1]);

// Scene 
const light = new THREE.AmbientLight( 0x404040 ); // soft white light
scene.add( light );



// Functions
function generateFunction(){
    let data = []

    // Generate a lj potential graph
    for (let i=0.93; i < 5.0 ; i+=0.1){
        data.push({x:i,y:lennardJones(i)});
    }
    return data
}
function graph(){
    let data = generateFunction()
    let xInitial = 2*initDist + 2*radius
    let Graph =  new Chart(ctx,  {
        type: 'line',
        data: {
          datasets: [{ 
            data: [{x:xInitial,y:lennardJones(xInitial)}],
            label: "Current State",
            borderColor: "white",
            fill: false
          },{ 
              data: data,
              label: "Lennard Jones Potential",
              borderColor: "#c45850",
              fill: false
            }
          ]
        },
        options: {
            scales: {
                xAxes: [{
                    type:'linear',
                    scaleLabel: {
                        display: true,
                        labelString: 'Distance'
                      }
                }],
                yAxes: [{
                    type:'linear',
                    scaleLabel: {
                        display: true,
                        labelString: 'Potential Energy'
                      }
                }]
            }
        }
      });

    return Graph

      
}


function lennardJones(i){
    return 4*epslon*((sigma/i)**12 - (sigma/i)**6)
}

function ljPotential(position){
    let x_Next = 0;
    let y_Next = 0;
    for (var i=0; i<position.length; i++) {
        // let x_next =  2* this.pos.x - this.oldPos.x + this.a.x* this.dt**2;
        x_Next = 2*position[i].x  - oldPosition[i].x + aceleration[i].x * dt * dt;
        oldPosition[i].x = position[i].x
        position[i].x = x_Next

        
        y_Next = 2*position[i].y  - oldPosition[i].y + aceleration[i].y * dt * dt;
        oldPosition[i].y = position[i].y
        position[i].y = y_Next
	}
    
    computeAceleration(position)

    sphere1.position.copy(position[0]);
    sphere2.position.copy(position[1]);
}

function computeAceleration(position){
    let dy = position[0].y - position[1].y 
    let dx = position[0].x - position[1].x 
    let r = Math.sqrt(dx*dx + dy*dy)
    let V = lennardJones(r);
    aceleration[0].x  =  V * dx
    aceleration[1].x  = -V * dx
    aceleration[0].y  =  V * dy
    aceleration[1].y  = -V * dy
}
function cameraMovement(){
    camP+=camV;
    camera.position.set( 0, 0, Math.sin(camP) + 5 );
    camera.lookAt( 0, 0, 0 );
}


function startPause(){
    running = !running;
    animate();
  
}

function reset(){
    initDist =  parseFloat(document.getElementById("initialD").value);
    oldPosition = [new THREE.Vector3(radius+initDist,0,0),new THREE.Vector3(-(radius+initDist),0,0)];
    position = [new THREE.Vector3(radius+initDist,0,0),new THREE.Vector3(-(radius+initDist),0,0)];
    velocity = [new THREE.Vector3(),new THREE.Vector3()];
    aceleration = [new THREE.Vector3(),new THREE.Vector3()];
    
}

function rotateObject(speed,object1,object2){
    const rotationSpeed = speed;
    object1.rotation.y += rotationSpeed;
    object2.rotation.y -= rotationSpeed;
    object1.rotation.x -= rotationSpeed;
    object2.rotation.x += rotationSpeed;
}

function distance(position){
    let dy = position[0].y - position[1].y 
    let dx = position[0].x - position[1].x 
    return Math.sqrt(dx*dx + dy*dy)
}


function updateGraph(Graph){
    let D = distance(position);
    if (D > 0.915){
        Graph.data.datasets[0].data[0] = {x:D,y:lennardJones(D)}
        Graph.update(0);
    }
    
}
Graph = graph()
function animate() {
    updateGraph(Graph)
    for (var step=0; step<stepsPerFrame; step++) {
        ljPotential(position)
        
        
    }
    
    //console.log(distance(position))
    
    rotateObject(0.01,sphere1,sphere2)
    
    
    // Camera movemente
    cameraMovement()

    if (running){
        requestAnimationFrame(animate)
    }
    
	renderer.render( scene, camera );
}



